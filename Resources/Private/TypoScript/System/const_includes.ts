# INCLUDE CSS-Styled-Content
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:fluid_styled_content/Configuration/TypoScript/Static/constants.txt">

# INCLUDE Main Setup
<INCLUDE_TYPOSCRIPT: source="FILE:./Frontend/const_pages.ts">

# INCLUDE Extension custom settings
<INCLUDE_TYPOSCRIPT: source="FILE:../Extensions/const_extensions.ts">

# vhs merge in short filename
plugin.tx_vhs.assets.mergedAssetsUseHashedFilename = 1

tt_content.gridelements_pi1.20.10.setup {
    contentWrapper.cObject.partialRootPaths.200 = {$plugin.tx_skin_paints.view.partialRootPath}
}

lib.math = TEXT
lib.math {
    current = 1
    prioriCalc = 1
}
